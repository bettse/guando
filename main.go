package main

import (
	"encoding/hex"
	"github.com/GeertJohan/go.hid"
	"gopkg.in/alecthomas/kingpin.v2"
	"log"
)

var done = make(chan struct{})

var (
	debug  = kingpin.Flag("debug", "Debug mode.").Short('d').Bool()
	color  = kingpin.Flag("color", "3 byte hex color code.").Short('c').Required().String()
	sound  = kingpin.Flag("sound", "Sound number [0-8].").Short('s').Default("0").Int()
	volume = kingpin.Flag("volume", "Volume [0-8].").Short('v').Default("0").Int()
)

func main() {
	kingpin.Parse()

	rgb, err := hex.DecodeString(*color)
	if err != nil || len(rgb) != 3 {
		if *debug {
			log.Println("Invalid color code format")
		}
		return
	}

	led, err := hid.Open(0x04D8, 0xF848, "")
	if err != nil {
		log.Fatalf("Could not open leds device: %s", err)
	}

	sendCommand(led, rgb, uint8(*sound), uint8(*volume))

	defer led.Close()

	<-done
	if *debug {
		log.Println("Done")
	}
}

func sendCommand(led *hid.Device, rgb []uint8, sound uint8, volume uint8) (int, error) {
	const sound_base = 0x80

	data := make([]byte, 64+1)
	data[0] = 0 // report ID
	data[1] = 0
	data[2] = 0
	data[3] = rgb[0]
	data[4] = rgb[1]
	data[5] = rgb[2]
	data[6] = 0
	data[7] = 0
	data[8] = sound_base + sound*8 + volume

	count, err := led.Write(data)
	if err != nil {
		log.Fatalf("Could not send feature report to do dummy action. %s\n", err)
		return count, err
	}
	if *debug {
		log.Println("Sent", count, "bytes")
	}
	return count, nil
}
