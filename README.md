# guando
CLI tool to control Kuando in Go

Based on the example at https://github.com/GeertJohan/go.hid and parts of https://git.dim13.org/redbutton.git/

The device will continue to play sounds/ringtones forever, but the light only remains on for about 10 seconds.  Neither of these are handled in the program right now
